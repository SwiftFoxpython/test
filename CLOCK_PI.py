import tkinter as tk
from datetime import datetime
from time import strftime, time, gmtime
import os
win = tk.Tk()
win.overrideredirect(True)
bg = "#009c24"
win.configure(bg=bg)
win.geometry("850x500")

initClock = 0
newGoal = ""
forceQuit = False


def add10():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock += 10
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)

def add20():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock += 20
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)

def add30():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock += 30
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)

def add60():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock += 60
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)



def sub10():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock -= 10
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)

def sub20():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock -= 20
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)

def sub30():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock -= 30
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)

def sub60():
    global initClock, newGoal
    win.config(bg="#ba0013")
    goalLabel.config(bg="#ba0013")
    timeLabel.config(bg="#ba0013")
    DNDtimer.config(text="Busy. DO NOT DISTURB UNTIL TIME BELOW.", bg="#ba0013")
    
    initClock -= 60
    dtime = int(time()) + initClock * 60
    newGoal = strftime("%H:%M", gmtime(dtime))
    goalLabel.config(text=newGoal)

def exitUI():
    os._exit(0)

def quitTimer():
    global forceQuit
    forceQuit = True

def getTime():
    global newGoal, forceQuit
    if strftime("%H%M") >= newGoal.replace(":","") or forceQuit:
        win.config(bg="#009c24")
        goalLabel.config(bg="#009c24", text="")
        timeLabel.config(bg="#009c24")
        DNDtimer.config(bg="#009c24", text="Free. Knock if needed!")
        initClock = 0
        forceQuit = False
    currentTime = strftime("%H:%M:%S")
    timeLabel.config(text = currentTime)
    timeLabel.after(1000, getTime)

 
  

timeLabel = tk.Label(win, font="Helvetica 80", bg=bg, fg="#fff")
DNDtimer = tk.Label(win, text="Free. Knock if needed!", font="Helvetica 50", bg=bg, fg="#fff")
goalLabel = tk.Label(win, font="Helvetica 80", bg=bg, fg="#fff")
exitButton = tk.Button(win, text="EXIT", font="Helvetica 10", bg="#ff6060", fg="#fff", command=exitUI)
quitButton = tk.Button(win, text="FORCE STOP", font="Helvetica 10", bg="#ff6060", fg="#fff", command=quitTimer)

disturb10 = tk.Button(win, text="+10", font="Helvetica 10", bg="#ff6060", fg="#fff", command=add10)
disturb20 = tk.Button(win, text="+20", font="Helvetica 10", bg="#ff6060", fg="#fff", command=add20)
disturb30 = tk.Button(win, text="+30", font="Helvetica 10", bg="#ff6060", fg="#fff", command=add30)
disturb60 = tk.Button(win, text="+60", font="Helvetica 10", bg="#ff6060", fg="#fff", command=add30)

_disturb10 = tk.Button(win, text="-10", font="Helvetica 10", bg="#ff6060", fg="#fff", command=sub10)
_disturb20 = tk.Button(win, text="-20", font="Helvetica 10", bg="#ff6060", fg="#fff", command=sub20)
_disturb30 = tk.Button(win, text="-30", font="Helvetica 10", bg="#ff6060", fg="#fff", command=sub30)
_disturb60 = tk.Button(win, text="-60", font="Helvetica 10", bg="#ff6060", fg="#fff", command=sub60)

timeLabel.place(relx=0.5, rely=0.2, anchor=tk.CENTER)
exitButton.place(relx=0.5, rely=0.9, anchor=tk.CENTER)
DNDtimer.place(relx=0.5, rely=0.4, anchor=tk.CENTER)

goalLabel.place(relx=0.5, rely=0.6, anchor=tk.CENTER)
disturb10.place(relx=0.6, rely=0.9, anchor=tk.CENTER)
disturb20.place(relx=0.7, rely=0.9, anchor=tk.CENTER)
disturb30.place(relx=0.8, rely=0.9, anchor=tk.CENTER)
disturb60.place(relx=0.9, rely=0.9, anchor=tk.CENTER)

_disturb10.place(relx=0.4, rely=0.9, anchor=tk.CENTER)
_disturb20.place(relx=0.3, rely=0.9, anchor=tk.CENTER)
_disturb30.place(relx=0.2, rely=0.9, anchor=tk.CENTER)
_disturb60.place(relx=0.1, rely=0.9, anchor=tk.CENTER)

quitButton.place(relx=0.5, rely=0.8, anchor=tk.CENTER)



getTime()
win.mainloop()
